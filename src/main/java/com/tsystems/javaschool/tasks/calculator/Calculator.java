package com.tsystems.javaschool.tasks.calculator;



import java.math.RoundingMode;
import java.text.DecimalFormat;

public class Calculator {

    private char currChar;
    private int currPos;
    private String initString;

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if (statement == null || statement.isEmpty()) {
            return null;
        }
        init(statement);
        getNextChar();
        DecimalFormat decimalFormat = new DecimalFormat("#.####");
        decimalFormat.setRoundingMode(RoundingMode.CEILING);
        try {
            double x = parseExpression();
            if (currPos < initString.length())
                return null;
            return decimalFormat.format(x).replace(',', '.');
        }
        catch (Exception e) {
            return null;
        }
    }

    /*
     * Initializing starting values for parses
     */
    private void init(String initString) {
        this.initString = initString;
        this.currChar = (char)-1;
        this.currPos = -1;

    }

    /*
     * Function trying to obtain next char, or saves -1
     */
    private void getNextChar() {
        currPos++;
        if (currPos < initString.length()) {
            currChar = initString.charAt(currPos);
        }
        else {
            currChar = (char)-1;
        }
    }

    /*
     * Function is looking for special next char
     */
    private boolean findChar(char req) {
        if (currChar == req) {
            getNextChar();
            return true;
        }
        return false;
    }


    /*   Next 3 functions are paring grammar objects
         grammar:
         expression = factor | expression '+' factor | expression '-' factor
         factor = essence | factor '/' essence | factor '*' essence |
         essence = '+' essence | '-' essence | number | '(' expression ')'
     */
    private double parseExpression() throws Exception {
        double x = parseFactor();
        while (true) {
            if (findChar('+')) {
                if (findChar('+'))
                    throw new Exception();
                x += parseFactor();
            }
            else if (findChar('-')) {
                if (findChar('-'))
                    throw new Exception();
                x -= parseFactor();
            }
            else
                return x;
        }
    }

    private double parseFactor() throws Exception {
        double x = parseEssence();
        while (true) {
            if (findChar('/')) {
                double pE = parseEssence();
                if (pE == 0)
                    throw new Exception();
                x /= pE;
            }

            else if (findChar('*')) {
                if (findChar('*'))
                    throw new Exception();
                x *= parseEssence();
            }
            else
                return x;
        }
    }

    private double parseEssence() throws Exception {
        if (findChar('+')) {
            return parseEssence();
        }
        if (findChar('-')) {
            return -parseEssence();
        }

        double x = 0;
        int startPos = currPos;
        if (findChar('(')) {
            x = parseExpression();
            if (!findChar(')'))
                throw new Exception();
        }
        else if ((currChar >= '0' && currChar <= '9') || currChar == '.') {
            while ((currChar >= '0' && currChar <= '9') || currChar == '.')
                getNextChar();
            x = Double.parseDouble(initString.substring(startPos, currPos));
        }

        return x;

    }

}
