package com.tsystems.javaschool.tasks.subsequence;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) throws IllegalArgumentException {
        int[] idx = {0};
        if (!(x instanceof List) || x == null)
            throw  new IllegalArgumentException();

        if (!(y instanceof List) || y == null)
            throw  new IllegalArgumentException();


        if (x.size() > y.size())
            return false;

        if (x.size() == 0)
            return true;


        AtomicInteger index = new AtomicInteger();
        Map<Integer, Object> mX = (Map<Integer, Object>) x.stream().collect(Collectors.toMap(i -> index.getAndIncrement(), i -> i));

        AtomicInteger index2 = new AtomicInteger();
        Map<Integer, Object> mY = (Map<Integer, Object>) y.stream().collect(Collectors.toMap(i -> index2.getAndIncrement(), i -> i));

        boolean bvFound = false;
        Iterator<Map.Entry<Integer, Object>> itY = mY.entrySet().iterator();
        while (itY.hasNext()) {
            Map.Entry<Integer, Object> pairY = itY.next();
            Set<Integer> foundKeys = getKeysByValue(mX, pairY.getValue());
            if (foundKeys.size() == 0) {
                itY.remove();
            }
        }

        //recalc mY indexex
        AtomicInteger index3 = new AtomicInteger();
        mY = (Map<Integer, Object>) mY.entrySet().stream().collect(Collectors.toMap(
                        i->index3.getAndIncrement(), i -> i.getValue() ));

        Iterator<Map.Entry<Integer, Object>> itX = mX.entrySet().iterator();
        while (itX.hasNext()) {
            bvFound = false;
            Map.Entry<Integer, Object> pairX = itX.next();
            Set<Integer> foundKeys = getKeysByValue(mY, pairX.getValue());

            for (Integer it : foundKeys) {
                if (it >= pairX.getKey()) {
                    bvFound = true;
                }
            }
            if (bvFound)
                itX.remove();

        }

        return mX.size() == 0;
    }

    /**
     * Helper function, which searches all occurenses of a value in a map
     * and return set with keys.
     *
     */
    public static <T, E> Set<T> getKeysByValue(Map<T, E> map, E value) {
        Set<T> keys = new HashSet<T>();
        for (Map.Entry<T, E> entry : map.entrySet()) {
            if (Objects.equals(value, entry.getValue())) {
                keys.add(entry.getKey());
            }
        }
        return keys;
    }
}
