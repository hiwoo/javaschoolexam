package com.tsystems.javaschool.tasks.pyramid;

import com.tsystems.javaschool.tasks.pyramid.additional.PyramidSize;
import com.tsystems.javaschool.tasks.pyramid.additional.TempSeqParams;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        PyramidSize pyramidSize = getPyramidSize(inputNumbers.size());

        if (!checkListLenght(inputNumbers.size()))
            throw new CannotBuildPyramidException();

        int nX = pyramidSize.getX();
        int nY = pyramidSize.getY();
        int[][] matrix = new int[nY][nX];

        //filling with 0
        for (int i = 0; i < nY; i++) {
            Arrays.fill(matrix[0], 0);
        }

        //sorting, try for handling nulls in array
        try {
            Collections.sort(inputNumbers);
        }
        catch (Exception e) {
            throw new CannotBuildPyramidException();
        }

        /*
            nShift = horizontal shift, increasing from bottom to the top
            nCnt = amount of values being used from input List
            nStartOffset = current offset of input list for evety line of matrix
            k = iterator of input List
            i, j = matrix iterators
        */

        for (int i = nY - 1, nCnt = 0, nShift = 0, nStartOffset = 0; i >= 0; i--, nShift++) {
            nStartOffset = inputNumbers.size() - nCnt - i - 1 ;
            for (int j = nShift, k = 0; j < nX - nShift; j+=2, k++, nCnt++) {
                matrix[i][j] = inputNumbers.get(nStartOffset + k);
            }
        }

        return matrix;

    }


    /**
     * Sequnce function, recursive:
     * 0, 1, 3, 6, 10, 15, 21, 28...
     * @param curVal - current value
     * @param maxVal - required value
     * @return boolean
     */
    private TempSeqParams runOverSequence(Integer curVal, Integer maxVal, Integer lastStep) {
        if (curVal <= maxVal - lastStep) {
            try {
                return runOverSequence(curVal + lastStep, maxVal, lastStep+1);
            }
            catch (StackOverflowError e){
                throw new CannotBuildPyramidException();
            }
        }
        else
            return new TempSeqParams(curVal, lastStep);
    }

    /**
     * Checks if if current size is suitable for available sizes from sequence
     *
     * @param length - length of input list
     * @return boolean
     */
    private boolean checkListLenght(int length) {
        return runOverSequence(0, length, 0).getCurrSize() == length;
    }

    /**
     * Calculates pyramid size from sequence, recursive:
     * sequence is like (x, y): 1x1, 3x2, 5x3..
     * @param size - length of input list
     * @return PyramidSize - extra class with (x, y) size of pyramid
     */
    private PyramidSize getPyramidSize(int size) {
        int x = 1, y = 1;
        for (int i = 0; i < runOverSequence(0, size, 0).getLastStep()-2; i++) {
            x += 2;
            y += 1;
        }
        return new PyramidSize(x, y);
    }
}
