package com.tsystems.javaschool.tasks.pyramid.additional;
/** Represents sizes of Pyramid

 */
public class PyramidSize {
    private int x, y;

    public PyramidSize(int x, int y) {
        this.x = x;
        this.y = y;
    }


    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
