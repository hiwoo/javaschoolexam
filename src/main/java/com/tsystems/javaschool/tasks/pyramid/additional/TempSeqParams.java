package com.tsystems.javaschool.tasks.pyramid.additional;

/** Helper class for reqursion call, to calculate sequence
 */

public class TempSeqParams {
    private  Integer currSize, lastStep;

    public TempSeqParams(Integer currSize, Integer lastStep) {
        this.currSize = currSize;
        this.lastStep = lastStep;
    }

    public Integer getCurrSize() {
        return currSize;
    }

    public Integer getLastStep() {
        return lastStep;
    }
}
